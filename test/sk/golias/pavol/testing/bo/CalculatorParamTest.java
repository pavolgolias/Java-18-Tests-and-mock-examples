package sk.golias.pavol.testing.bo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sk.golias.pavol.testing.calc.Calculator;
import sk.golias.pavol.testing.calc.CalculatorImpl;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(org.junit.runners.Parameterized.class)
public class CalculatorParamTest {
    private int num1;
    private int num2;
    private int expectedResult;

    public CalculatorParamTest(int num1, int num2, int expectedResult) {
        this.num1 = num1;
        this.num2 = num2;
        this.expectedResult = expectedResult;
    }

    @Parameterized.Parameters
    public static Collection<Integer[]> data() {
        return Arrays.asList(new Integer[][]{{-1, 2, 1}, {1, 2, 3}, {6, 7, 13}});
    }

    @Test
    public void addResultTest() {
        Calculator c = new CalculatorImpl();
        int result = c.add(num1, num2);
        assertEquals(expectedResult, result);
    }
}
