﻿## Java JUnit tests and mockito examples
Examples are based on this [tutorial](https://www.udemy.com/junitandmockitocrashcourse/).
 
 ***Best practises:***
 * **F.I.R.S.T** (fast - independent - repeatable - self-validating - timely)
 * **Test Doubles** - dummy, stubs, mocks, fake, spies
 
 
 ***Interesting:***
 * **PowerMock** - enables to test static classes and methods (which Mockito can not test) 
