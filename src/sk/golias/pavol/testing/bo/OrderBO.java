package sk.golias.pavol.testing.bo;

import sk.golias.pavol.testing.bo.exception.BOException;
import sk.golias.pavol.testing.dto.Order;

public interface OrderBO {

    boolean placeOrder(Order order) throws BOException;

    boolean cancelOrder(int id) throws BOException;

    boolean deleteOrder(int id) throws BOException;
}
