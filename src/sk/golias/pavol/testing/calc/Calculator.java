package sk.golias.pavol.testing.calc;

public interface Calculator {
    int add(int num1, int num2);
}
